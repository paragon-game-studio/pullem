using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoyShape : LocalSingleton<BoyShape>
{
    public SkinnedMeshRenderer rend;
    public void BlandShape(bool Up)
    {
        StartCoroutine(BShape(Up));
    }
    IEnumerator BShape(bool Up)
    {
        for (int i = 0; i < 100; i += 5)
        {
            yield return new WaitForSeconds(.05f);
            rend.SetBlendShapeWeight(0, i);
            rend.SetBlendShapeWeight(1, i);
            rend.SetBlendShapeWeight(2, i);
            rend.SetBlendShapeWeight(3, i);
            rend.SetBlendShapeWeight(4, i);
        }
        yield return new WaitForSeconds(2.5f);
        for (int i = 100; i > 0; i -= 7)
        {
            yield return new WaitForSeconds(.05f);
            rend.SetBlendShapeWeight(0, i);
            rend.SetBlendShapeWeight(1, i);
            rend.SetBlendShapeWeight(2, i);
            rend.SetBlendShapeWeight(3, i);
            rend.SetBlendShapeWeight(4, i);
        }
    }
}
