using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectsPool : LocalSingleton<ObjectsPool>
{
    public Renderer rend;
    public Color[] fireColor;

    GameObject boyPhysical;
    ConfigurableJoint[] conf;
    CopyLimb[] copy;

    private Queue<GameObject> objectsInPool;
    public GameObject[] poolObjects;

    public Material mat;
    public ParticleSystem particle;
    public ParticleSystem extraParticle;
    public bool particleOrObject;
    public bool heavyObject;
    public bool fire;
    public bool water;
    private void Awake()
    {
        rend = LevelManager.Instance.boyMesh.GetComponent<Renderer>();
        boyPhysical = GameManager.Instance.boyPhysical;
        conf = boyPhysical.GetComponentsInChildren<ConfigurableJoint>();
        copy = boyPhysical.GetComponentsInChildren<CopyLimb>();
        objectsInPool = new Queue<GameObject>();
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].SetActive(false);
            objectsInPool.Enqueue(poolObjects[i]);
        }

    }
    public void PoolActive()
    {
        GameManager.Instance.time = 5;
        StartCoroutine(SmoothActive());
        Boy.Instance.mat = mat;
        if (water || fire)
        {
            GameManager.Instance.time = 8;
            if (water)
            {
                Boy.Instance.RandOn(20,2.5f);
            }
            
        }
        if (particle != null && !water && !fire)
        {
            Boy.Instance.particle = particle;
        }
        if (heavyObject)
        {
            BoyPhysicalInvoke(100, false, 4);
            GameManager.Instance.time = 9;

        }
    }
    IEnumerator SmoothActive()
    {
        yield return new WaitForSeconds(2.5f);
        if (!particleOrObject)
        {
            if (heavyObject)
            {
                BoyShape.Instance.BlandShape(true);
            }
            for (int i = 0; i < poolObjects.Length; i++)
            {
                yield return new WaitForSeconds(.1f);
                poolObjects[i].SetActive(true);
            }
            yield return new WaitForSeconds(3f);
            for (int i = 0; i < poolObjects.Length; i++)
            {
                poolObjects[i].transform.position = gameObject.transform.position;
                poolObjects[i].SetActive(false);
            }
        }
        else
        {
            if (fire || water)
            {
                BoyShape.Instance.BlandShape(true);
            }
            if (fire)
            {
                rend.materials[0].DOColor(fireColor[0], 3);
                rend.materials[1].DOColor(fireColor[1], 3);
                rend.materials[6].DOColor(fireColor[1], 3);
                rend.materials[7].DOColor(fireColor[2], 3);
                rend.materials[9].DOColor(fireColor[3], 3);
            }
            particle.Play();
            if (extraParticle != null)
            {
                extraParticle.Play();
            }
        }

    }
    public void BoyPhysicalInvoke(int value, bool limB, float time)
    {
        if (GameManager.Instance.memoryRandom.Count == GameManager.Instance.finishButtonIndex-GameManager.Instance.maxButtonIndex)
        {
            StartCoroutine(BoyPhysicalFinish(value, limB, time));
        }
        else
        {
            StartCoroutine(BoyPhysical(value, limB, time));
        }
    }
    IEnumerator BoyPhysical(int value, bool limB, float time)
    {
        yield return new WaitForSeconds(time);
        Boy.Instance.hips.GetComponent<Rigidbody>().isKinematic = false;
        for (int i = 0; i < 2; i++)
        {
            foreach (ConfigurableJoint c in conf)
            {
                JointDrive spring = c.slerpDrive;
                spring.mode = JointDriveMode.Position;
                spring.positionSpring = value;
                c.slerpDrive = spring;
            }
            foreach (CopyLimb cL in copy)
            {
                cL.enabled = limB;
            }
            yield return new WaitForSeconds(time / 2);
            value = 350;
            limB = true;
            AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.�dle);
        }
        yield return new WaitForSeconds(time / 8);
        Boy.Instance.BoySid();
    }
    IEnumerator BoyPhysicalFinish(int value, bool limB, float time)
    {
        Boy.Instance.hips.transform.DOLocalMove(new Vector3(0, .85f, 0),1f).SetDelay(time);
        foreach (ConfigurableJoint c in conf)
        {
            JointDrive spring = c.slerpDrive;
            spring.mode = JointDriveMode.Position;
            spring.positionSpring = value;
            c.slerpDrive = spring;
        }
        foreach (CopyLimb cL in copy)
        {
            cL.enabled = limB;
        }
        yield return new WaitForSeconds(time);
    }
}
