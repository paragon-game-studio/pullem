using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boy : LocalSingleton<Boy>
{
    public GameObject animated;
    public GameObject hips;
    public Transform chair;
    public Transform collisionPoint;
    public ParticleSystem particle;
    public ParticleSystem particle2;
    public GameObject[] dirt;
    public List<GameObject> memory;
    public Material mat;
    int lastDirt;
    private void Awake()
    {
        for (int i = 0; i < dirt.Length; i++)
        {
            dirt[i].SetActive(false);
        }
        BoySid();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Particle"))
        {
            RandOn(5,0);
            collision.gameObject.SetActive(false);
            collisionPoint.transform.position = collision.gameObject.transform.position;
            particle.Play();
        }
        else if (collision.gameObject.CompareTag("Heavy"))
        {
            RandOn(5,0);
        }
    }
    public void RandOn(int loopLenght, float time)
    {
        StartCoroutine(RandomOn(loopLenght,time));
    }
    IEnumerator RandomOn(int loopLenght, float time)
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < loopLenght; i++)
        {
            yield return new WaitForSeconds(.05f);
            int random = Random.Range(0, dirt.Length);
            dirt[random].GetComponent<Renderer>().material = mat;
            dirt[random].SetActive(true);
            memory.Add(dirt[random]);
        }
    }
    //public void OnOff(bool value)
    //{
    //    for (int i = 0; i < memory.Count; i++)
    //    {
    //        memory[i].SetActive(value);
    //    }
    //}

    public void BoySid()
    {
        chair.GetComponent<Collider>().enabled = false;
        hips.GetComponent<Rigidbody>().isKinematic = true;
        hips.transform.DOMove(chair.transform.GetChild(0).position, .8f).OnComplete(() => { chair.GetComponent<Collider>().enabled = true; });
        hips.transform.DOMove(chair.transform.GetChild(1).position, .8f).SetDelay(.8f);
        hips.transform.DORotate(Vector3.zero, 1);
    }
}
