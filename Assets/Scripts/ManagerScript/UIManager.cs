using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIManager : LocalSingleton<UIManager>
{
    public GameObject startPanel;
    public GameObject nextPanel;
    public GameObject alphaPanel;
    public GameObject escaped;
    public TextMeshProUGUI level;
    int levelIndex = 1;

    public bool start;

    private void Awake()
    {
        levelIndex = Mathf.Clamp(levelIndex, 1, 999);
        PlayerPrefs.SetInt("Level", levelIndex);
    }
    private void Start()
    {
        levelIndex = PlayerPrefs.GetInt("Level");
        level.text = "Level: " + levelIndex.ToString();
        StartCoroutine(StartAlphaPanel());
    }
    public void GamePlay()
    {
        startPanel.transform.DOScale(Vector3.zero, .3f).SetEase(Ease.Linear);
    }
    public void NextLevel()
    {
        nextPanel.transform.DOScale(Vector3.one, .3f).SetEase(Ease.Linear);
        escaped.transform.DOScale(Vector3.one, .3f).SetEase(Ease.Linear);
        StartCoroutine(AlphaPanel());
    } 
    IEnumerator AlphaPanel()
    {
        for (float i = 0; i < 100; i++)
        {
            yield return new WaitForSeconds(0.04f);
            alphaPanel.transform.GetComponent<CanvasGroup>().alpha = (i/99);
            if (i > 98)
            {
                levelIndex++;
                PlayerPrefs.SetInt("Level", levelIndex);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    } 
    IEnumerator StartAlphaPanel()
    {
        for (float i = 100; i > 0; i--)
        {
            yield return new WaitForSeconds(0.03f);
            alphaPanel.transform.GetComponent<CanvasGroup>().alpha = (i/99);
            if (i < 2)
            {
                nextPanel.transform.DOScale(Vector3.zero, .3f).SetEase(Ease.Linear);
            }
        }
    }


}
