using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelManager : LocalSingleton<LevelManager>
{
    public GameObject boyMesh;
    [SerializeField] private GameObject camera;
    [SerializeField] private Vector3 cameraPos1, cameraPos2, cameraPos3;
    [SerializeField] private Vector3 cameRaot;

    void Start()
    {
        camera = Camera.main.gameObject;
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.�dle);
        Selection();
    }
    public void Selection()
    {
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.�dle);
        GameManager.Instance.isWatch = true;
        camera.transform.DOLocalMove(cameraPos1, .7f);
        camera.transform.DOLocalRotate(new Vector3(0, 180, 0), .5f).OnComplete(()=> { GameManager.Instance.buttonHit = false; /*boyMesh.SetActive(false); Boy.Instance.OnOff(false); */});
    }
    public void Watch()
    {
        StartCoroutine(WatchWait());
    } 
    public void FinishWatch()
    {
        camera.transform.DOLocalMove(cameraPos3, 1f);
        camera.transform.DOLocalRotate(cameRaot, 1f);
        //camera.transform.DOLocalRotate(cameRaot, 1f);
    }
    IEnumerator WatchWait()
    {
        yield return new WaitForSeconds(.8f);
        //boyMesh.SetActive(true);
        //Boy.Instance.OnOff(true);
        yield return new WaitForSeconds(.7f);
        camera.transform.DOLocalMove(cameraPos2, 1f);
        camera.transform.DOLocalRotate(new Vector3(0, 180, 0), 1f);
        yield return new WaitForSeconds(.8f);
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.startDefance,1,.3f);
        yield return new WaitForSeconds(.5f);
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.defance,1,.3f);
        
    }
}
