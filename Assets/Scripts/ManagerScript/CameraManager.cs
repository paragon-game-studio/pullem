using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraManager : LocalSingleton<CameraManager>
{
    public float sens;
    public float xRot;
    public float yRot;
    private void Start()
    {
        yRot = 180;
    }
    public void RotateCamera()
    {
        float rotX = Input.GetAxisRaw("Mouse X") * sens * Time.deltaTime;
        float rotY = Input.GetAxisRaw("Mouse Y") * sens * Time.deltaTime;
        xRot -= rotY;
        yRot += rotX;
        xRot = Mathf.Clamp(xRot, -10, 10);
        yRot = Mathf.Clamp(yRot, 140, 220);
        transform.localRotation = Quaternion.Euler(xRot, yRot, 0);
    }
}
