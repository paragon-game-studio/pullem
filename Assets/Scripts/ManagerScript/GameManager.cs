using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class GameManager : LocalSingleton<GameManager>
{
    public LayerMask mask;
    public GameObject boyPhysical;
    [SerializeField] private TextMeshPro text;
    public GameObject[] poolObjects;
    public GameObject nextLevel;
    public List<int> memoryRandom;
    
    RaycastHit hit;
    Ray ray;

    public bool isWatch = true;
    public bool levelFinish = false;
    public bool buttonHit = false;
    public bool finish = false;

    public float maxRange = 25;
    public float time = 6;
    public float finishButtonIndex;
    public int maxButtonIndex = 0;
    void Start()
    {
        finishButtonIndex = poolObjects.Length;
        maxButtonIndex = Random.Range(0, (int)finishButtonIndex-5);
        memoryRandom = new List<int>();
        StartCoroutine(TextScale());
    }
    void Update()
    {
        if (!buttonHit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!UIManager.Instance.start)
                {
                    UIManager.Instance.start = false;
                    UIManager.Instance.GamePlay();
                }
                maxRange = 25;
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Invoke("ResetRey", .15f);
            }
            else if (Input.GetMouseButton(0) && isWatch)
            {
                CameraManager.Instance.RotateCamera();
            }
            else if (Input.GetMouseButtonUp(0) && !finish)
            {
                if (Physics.Raycast(ray, out hit, maxRange, mask))
                {
                    if (hit.collider.gameObject.CompareTag("Button"))
                    {
                        ButtonHit(hit.collider.gameObject);
                    }
                }

            }
        }
      
    }
    void ResetRey()
    {
        maxRange = 0;
    }
    #region
    void ButtonHit(GameObject button)
    {
        buttonHit = true;
        int random = RandomPoolObjects();
        if (memoryRandom.Count == finishButtonIndex-maxButtonIndex)
        {
            levelFinish = true;
        }
        if (!levelFinish)
        {
            //Camera.main.transform.DOMove(button.transform.GetChild(2).transform.position, .8f);
            //Camera.main.transform.DOLocalRotate(button.transform.GetChild(2).transform.rotation.eulerAngles, .5f);
            button.transform.GetChild(0).transform.DOLocalMoveY(-0.05f, .3f);//.SetDelay(.7f);
            button.transform.GetChild(0).transform.DOLocalMoveY(0f, .3f).SetDelay(.3f); ;//.SetDelay(.9f);
            button.GetComponent<Collider>().enabled = false;
            poolObjects[random].GetComponent<ObjectsPool>().PoolActive();
            text.text = poolObjects[random].gameObject.name;
            LevelManager.Instance.Watch();
            isWatch = false;
            Invoke("SelectionTransition", time);
        }
        else
        {
            StartCoroutine(Finish(button));
        }
      

    }
    IEnumerator Finish(GameObject button)
    {
        buttonHit = true;
        finish = true;
        Boy.Instance.hips.GetComponent<Rigidbody>().isKinematic = false;
        ParticleSystem puf;
        puf = nextLevel.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        ObjectsPool.Instance.BoyPhysicalInvoke(10000, true, 2.5f);
       // Boy.Instance.OnOff(true);
        Boy.Instance.chair.GetComponent<Collider>().enabled = false;
        //LevelManager.Instance.boyMesh.SetActive(true);
        //Camera.main.transform.DOMove(button.transform.GetChild(2).transform.position, .5f);
        //Camera.main.transform.DOLocalRotate(button.transform.GetChild(2).transform.rotation.eulerAngles, .3f);
        button.transform.GetChild(0).transform.DOLocalMoveY(-0.05f, .1f).SetDelay(.5f);
        button.transform.GetChild(0).transform.DOLocalMoveY(0f, .1f).SetDelay(.8f);
        yield return new WaitForSeconds(1);
        LevelManager.Instance.FinishWatch();
        yield return new WaitForSeconds(1.2f);
        puf.Play();
        nextLevel.transform.DORotate(new Vector3(-89.5f, 0, 0), 1);
        nextLevel.transform.GetChild(1).gameObject.SetActive(false);
        Boy.Instance.chair.transform.DORotate(new Vector3(0, 180, 0), 1);
        
        //Boy.Instance.hips.transform.DORotate(new Vector3(0, 180, 0), 1);
        yield return new WaitForSeconds(1);
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.upIdle,1,.3f);
        yield return new WaitForSeconds(.5f);
        AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.walk, 1, .3f);
        Boy.Instance.animated.transform.parent.transform.DOMove(new Vector3(0, 0, -2f), 1).SetDelay(.1f).OnComplete(()=>
        {
            AnimationManager.Instance.BoyPlayAnim(AnimationManager.Instance.dance, 1, .3f);
            Boy.Instance.particle2.Play();
        });
        yield return new WaitForSeconds(.5f);
        UIManager.Instance.NextLevel();
    }
    void SelectionTransition()
    {
        CameraManager.Instance.yRot = 180;
        CameraManager.Instance.xRot = 0;
        if (!levelFinish)
        {
            LevelManager.Instance.Selection();
        }
        
    }
    int RandomPoolObjects()
    {
        int random = Random.Range(0, poolObjects.Length);
        if (memoryRandom.Count <= poolObjects.Length && memoryRandom.Count > 0)
        {
            for (int a = 0; a < 7; a++)
            {
                for (int i = 0; i < memoryRandom.Count; i++)
                {
                    while (memoryRandom[i] == random)
                    {
                        random = Random.Range(0, poolObjects.Length);
                    }
                }
            }
        }
        //if (memoryRandom.Count == 2)
        //{
        //    random = 9;
        //}
        //if (memoryRandom.Count == 0)
        //{
        //    random = 11;
        //}
        //if (memoryRandom.Count == 1)
        //{
        //    random = 10;
        //}
        memoryRandom.Add(random);
        return random;
    }
    #endregion
    IEnumerator TextScale()
    {
        while (true)
        {
            text.gameObject.transform.DOScale(new Vector3(.85f, .85f, .85f), .4f);
            yield return new WaitForSeconds(.4f);
            text.gameObject.transform.DOScale(Vector3.one, .4f);
            yield return new WaitForSeconds(.4f);
        }
    }
}
