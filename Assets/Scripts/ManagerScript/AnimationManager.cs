using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class AnimationManager : LocalSingleton<AnimationManager>
{
    [SerializeField] private AnimancerComponent boyAnimancer;
    public AnimationClip �dle, defance, startDefance, toGetUp, walk, upIdle,dance;
    public void PlayAnim(AnimancerComponent animancer, AnimationClip clip, float speed, float fade)
    {
        var state = animancer.Play(clip, fade);
        state.Speed = speed;
    }
    public void BoyPlayAnim(AnimationClip clip,float speed = .7f,float fade = 2f)
    {
        PlayAnim(boyAnimancer, clip, speed, fade);
    }
}
